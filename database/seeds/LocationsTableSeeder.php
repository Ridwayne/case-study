<?php

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Let's truncate our existing records to start from scratch.
         Location::truncate();

         $faker = \Faker\Factory::create();

         // And now, let's create a few locations in our database:
         for ($i = 0; $i < 10; $i++) {
             Location::create([
                 'name' => $faker->word,
                 'address' => $faker->sentence,
             ]);
    }
}
}
