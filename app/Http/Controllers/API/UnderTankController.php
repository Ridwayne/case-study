<?php

namespace App\Http\Controllers\API;

use App\Models\UnderTank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Location;

class UnderTankController extends Controller
{
    public function index()
    {
        return UnderTank::all();
    }

    public function show(UnderTank $undertank)
    {
        $undertank = UnderTank::where('undertank', '=', $undertank)->with('locations')->first();
        return $undertank;
    }


    public function store(Request $request)
    {
        $undertank = UnderTank::create($request->all());

        // $location = Location::find(['$id']);
        // $undertank->locations()->attach($location);

        return response()->json($undertank, 201);
    }

    public function update(Request $request, UnderTank $undertank)
    {
        $undertank->update($request->all());

        return response()->json($undertank, 200);
    }

    public function delete(UnderTank $undertank)
    {
        $undertank->delete();

        return response()->json(null, 204);
    }

    public function locations ()
    {
        return $this->belongsToMany(UnderTank::class, 'location_under_tank');
    }
}
