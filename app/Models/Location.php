<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';

    protected $fillable = ['name', 'address'];

    public function undertanks()
    {
        return $this->belongsToMany('App\Models\UnderTank', 'location_under_tank', 'location_id', 'under_tank_id');
    }
}
