<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnderTank extends Model
{
    protected $table = 'locations';

    protected $fillable = ['name', 'liters'];

    public function locations()
    {
        return $this->belongsToMany('App\Models\location', 'location_under_tank', 'under_tank_id', 'location_id');
    }
}
