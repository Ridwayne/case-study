<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/locations', 'API\LocationController@index');
Route::get('/locations/{location}', 'API\LocationController@show');
Route::post('/locations', 'API\LocationController@store');
Route::put('/locations/{location}', 'API\LocationController@update');
Route::delete('/locations/{location}', 'API\LocationController@delete');


Route::get('/undertanks', 'API\UnderTankController@index');
Route::get('/undertanks/{undertank}', 'API\UnderTankController@show');
Route::post('/undertanks', 'API\UnderTankController@store');
Route::put('/undertanks/{undertank}', 'API\UnderTankController@update');
Route::delete('/undertanks/{undertank}', 'API\UnderTankController@delete');
